@component('mail::message')
# {{$name}}

Kami mencatat bahwa Anda telah meminta untuk mereset password akun Anda. Untuk melanjutkan proses reset password, silakan klik pada tautan di bawah ini<br>

@component('mail::button', ['url' => $link])
Reset Password
@endcomponent

Jika Anda tidak merasa melakukan permintaan ini, mohon abaikan email ini. Namun, jika Anda memang meminta reset password, pastikan untuk segera mengikuti tautan tersebut agar dapat mengganti password akun Anda dengan yang baru.<br>

Atas perhatiannya, Terima kasih<br>
{{ config('app.name') }}
@endcomponent
