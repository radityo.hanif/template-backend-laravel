<?php

return [
  'IS_PROD' => env('APP_ENV') == 'production',
];