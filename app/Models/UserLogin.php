<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class UserLogin extends AAAModel
{
    use HasFactory;

    protected $casts = [
        "deleted_at" => 'datetime:Y-m-d H:m:s',
        "updated_at" => 'datetime:Y-m-d H:m:s',
        "created_at" => 'datetime:Y-m-d H:m:s',
    ];

    const updateableFields = [
        "login_name",
        "login_email",
        "login_password",
    ];

    const aliases = [
        "login_name" => "Nama Course",
        "login_email" => "Nama Guru",
        "login_password" => "Status Aktif Course",
    ];

    public static function isEmailAvailable($email)
    {
        $model = self::where("login_email", $email)->first();
        if (!$model) {
            return true;
        }
        return false;
    }
}
