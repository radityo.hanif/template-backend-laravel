<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AAAModel extends Model
{
  protected $casts = [
    "deleted_at" => 'datetime:Y-m-d H:m:s',
    "updated_at" => 'datetime:Y-m-d H:m:s',
    "created_at" => 'datetime:Y-m-d H:m:s',
  ];

  const searchableFields = [];
  const updateableFields = [];
  const aliases = [];
  const idField = "id";

  public static function defaultValidation($params, $id = false)
  {
    return false;
  }

  public static function formatting($data)
  {
    return $data;
  }

  public static function defaultFormatting($datas)
  {
    try {
      foreach ($datas as $index => $data) {
        $datas[$index] = self::formatting($data);
      }
    } catch (\Exception $e) {
      $datas = self::formatting($datas);
    }

    return $datas;
  }

  public static function format($data)
  {
    return $data;
  }

  public static function formatParams($params)
  {
    return $params;
  }

  public static function isAvailable($field, $param, $id = false)
  {
    $model = self::where($field, $param);
    if ($id) {
      $model = $model->where(self::idField, "!=", $id);
    }
    $model = $model->first();
    if (!$model) {
      return true;
    }
    return false;
  }
}
