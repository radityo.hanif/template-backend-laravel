<?php

namespace App\Http\Controllers;

use App\Util;
use App\Models\File;
use Illuminate\Http\Request;

/**
 * @group File
 *
 * APIs for managing files
 * @authenticated
 */
class FileController extends Controller
{
    /**
     * @bodyParam file file gambar/dokumen/file yang ingin diupload required.
     */
    public function create(Request $request)
    {
        // validasi data input
        if ($request->file("file") == null) {
            return Util::defaultResult("file belum diupload", 400);
        }

        // inisiasi meta data file
        $fileInput = [
            "name" => $request->file("file")->getClientOriginalName(),
            "extension" => $request->file("file")->getClientOriginalExtension(),
            "size" => Util::formatBytes($request->file("file")->getSize()),
            "sizeRaw" => $request->file("file")->getSize()
        ];

        // validasi : ukuran file (https://www.gbmb.org/mb-to-bytes )
        if ($fileInput["sizeRaw"] > 2097152) {
            return Util::defaultResult("gagal, ukuran file lebih besar dari 2 MB", 400, false, [
                "fileSize" => $fileInput["size"],
            ]);
        }

        // validasi : format file
        $validExtensions = ["pdf", "png", "jpg", "jpeg", "json"];
        if (!in_array($fileInput["extension"], $validExtensions)) {
            return Util::defaultResult("format file " . $fileInput["extension"] . " tidak diizinkan", 400, false, [
                "validExtensions" => $validExtensions,
            ]);
        }

        // inisiasi data lainnya
        $payload = Util::getJWTPayload();
        $loginFk = Util::getArrOrObject($payload, "login_id");

        // mapping data ke dalam model  
        $model = new File();
        $model->file_path = $request->file("file")->store("uploads");
        $model->file_login_fk = $loginFk;
        if (!$loginFk) {
            $model->file_is_temp = 1;
        }
        $model->file_size = $fileInput["size"];
        $model->file_name = $fileInput["name"];
        $model->file_keychar = File::generateFileKeychar();
        $model->save();
        return Util::defaultResult("berhasil mengupload file", 200, false, [
            "id" => $model->file_keychar
        ]);
    }

    public function readOne($keychar)
    {
        $model = File::where("file_keychar", $keychar)->first();
        if (!$model) {
            return Util::defaultResult("file tidak ditemukan", 404);
        }
        $model->file_url_download = File::getFileUrlByKeychar($keychar);
        $model->is_image = File::isImageFile($model->file_name);
        return Util::defaultResult("file berhasil ditemukan", 200, $model);
    }
}
