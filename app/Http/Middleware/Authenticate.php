<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        $headers = $request->header();

        // Cek apakah client memiliki token
        if (!isset($headers["authorization"])) {
            return response(["message" => "kamu tidak memiliki token"], 400);
        }

        // Cek apakah token milik client valid
        $clientToken = $headers["authorization"][0];
        $clientToken = explode(" ", $clientToken)[1]; // ambil token format token {bearer $token}
        try {
            JWT::decode($clientToken, new Key(env("JWT_SECRET"), 'HS256'));
        } catch (\Exception $e) {
            return response(["message" => "token tidak valid"], 401);
        }

        // Token valid, monggo lanjut..
        return $next($request);
    }
}
